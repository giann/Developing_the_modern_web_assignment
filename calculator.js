window.onload = init;

function calc() { 	
	var total;
	setDisplay(total);
}

function init() {

	var displayingValue="";
	var arrayNum = [];
	var arrayEquation = [];
	var temp="";
	var error=false;
	var result="";
	var checking = false;
	
	//define button 7 
	var seven = document.getElementById('7');
		seven.onclick = function(){
		displayingValue += "7"; 
		temp += "7";
		setDisplay(displayingValue);
		setResult("");
	}
	//define button 8
	var eight = document.getElementById('8');
		eight.onclick = function(){
		displayingValue += "8"; 
		temp += "8";
		setDisplay(displayingValue);
		setResult("");
	}
	//define button 9
	var nine = document.getElementById('9');
	nine.onclick = function(){
		displayingValue += "9";
		temp += "9";		
		setDisplay(displayingValue);
		setResult("");
	}
	//define button 4
	var four = document.getElementById('4');
		four.onclick = function(){
		displayingValue += "4";
		temp += "4";		
		setDisplay(displayingValue);
		setResult("");
	}
	//define button 5
	var five = document.getElementById('5');
		five.onclick = function(){
		displayingValue += "5"; 
		temp += "5";
		setDisplay(displayingValue);
		setResult("");
	}
	//define button 6
	var six = document.getElementById('6');
		six.onclick = function(){
		displayingValue += "6";
		temp += "6";		
		setDisplay(displayingValue);
		setResult("");
	}
	//define button 1
	var one = document.getElementById('1');
		one.onclick = function(){
		displayingValue += "1";
		temp += "1";		
		setDisplay(displayingValue);
		setResult("");
	}
	//define button 2
	var two = document.getElementById('2');
		two.onclick = function(){
		displayingValue += "2";
		temp += "2";
		setDisplay(displayingValue);
		setResult("");
	}
	//define button 3
	var three = document.getElementById('3');
		three.onclick = function(){
		displayingValue += "3";
		temp += "3";
		setDisplay(displayingValue);
		setResult("");
	}
	
	var zero = document.getElementById('0');
		zero.onclick = function(){
		displayingValue += "0";
		temp += "0";		
		setDisplay(displayingValue);
		setResult("");
	}
	//clear button for clear all value
	var clear = document.getElementById('C');
	clear.onclick = function(){
		arrayNum = [];
		arrayEquation = [];
		result="";
		displayingValue="";
		temp="";
		error=false;
		setDisplay("");
		setResult("");
	}
	//plus button
	var plus = document.getElementById('+');
	plus.onclick = function(){
		
		if(temp!="">0){
			arrayNum.push(temp);
			arrayEquation.push("+");
			displayingValue += "+";	
			setDisplay(displayingValue);
			temp="";
		}else{
			error=true;
		}
		
			
		

	}
	//Minus button
	var minus = document.getElementById('-');
	minus.onclick = function(){
		if(temp!=""){
			arrayNum.push(temp);
			arrayEquation.push("-");
			temp="";
		}else{
			error=true;
		}
		displayingValue += "-"; 
		
		setDisplay(displayingValue);

	}
	// multiplied button
	var multiplied = document.getElementById('x');
	multiplied.onclick = function(){
		if(temp!=""){
			arrayNum.push(temp);
			arrayEquation.push("*");
			temp="";
		}else{
			error=true;
		}
		displayingValue += "*"; 

		setDisplay(displayingValue);

	}
	//Divided button
		var divided = document.getElementById('/');
	divided.onclick = function(){
		if(temp!=""){
			arrayNum.push(temp);
			arrayEquation.push("/");
			temp="";
		}else{
			error=true;
		}
		displayingValue += "/";
		setDisplay(displayingValue);

	}
	
	
//calculation function
    Calc.onclick = function(){
		if(temp!=""){
			arrayNum.push(temp);	
			temp="";
		}else{
			error=true;
		}
		
		var index=0;
		var length=arrayNum.length; //define length as array length
		
		arrayNum.forEach(function(number) { //for looping all object
				switch(arrayEquation[index]){ 
					case "+" : result=parseInt(arrayNum[index]) + parseInt(arrayNum[index+1]);
					break;
					case "-" : result=parseInt(arrayNum[index]) - parseInt(arrayNum[index+1]);
					break;
					case "*" : result=parseInt(arrayNum[index]) * parseInt(arrayNum[index+1]);
					break;
					case "/" : result=parseInt(arrayNum[index]) / parseInt(arrayNum[index+1]);
					break;
					
				}	
			index++;
			
		});
		
		setResult("=" + result);
		
		result="";
		displayingValue="";
		arrayNum = [];
		arrayEquation = [];
		temp="";
		error=false;	
	}

}

function setDisplay(value){ //set HTML display value 
	document.getElementById('display').value = value;
}

function setResult(value){ //set result for display
	document.getElementById('result').value = value;
}

